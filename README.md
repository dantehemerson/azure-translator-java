# Azure Translator

> Ejemplo de un traductor usando la API de Traduccion de Texto de Microsoft Azure.

![Screenshot](.github/screenshot.png)

## License

[GPL 3.0 License](./LICENSE)
