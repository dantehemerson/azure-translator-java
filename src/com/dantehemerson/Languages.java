package com.dantehemerson;

import java.util.HashMap;
import java.util.Map;

public class Languages {
    private static Languages instace = null;
    private Map<String, String> languagesMap;

    private Languages() {
        languagesMap = new HashMap<>();

        languagesMap.put("af","Afrikaans");
        languagesMap.put("ar","Arabic");
        languagesMap.put("bg","Bulgarian");
        languagesMap.put("bn","Bangla");
        languagesMap.put("bs","Bosnian");
        languagesMap.put("ca","Catalan");
        languagesMap.put("cs","Czech");
        languagesMap.put("cy","Welsh");
        languagesMap.put("da","Danish");
        languagesMap.put("de","German");
        languagesMap.put("el","Greek");
        languagesMap.put("en","English");
        languagesMap.put("es","Spanish");
        languagesMap.put("et","Estonian");
        languagesMap.put("fa","Persian");
        languagesMap.put("fi","Finnish");
        languagesMap.put("fil","Filipino");
        languagesMap.put("fj","Fijian");
        languagesMap.put("fr","French");
        languagesMap.put("he","Hebrew");
        languagesMap.put("hi","Hindi");
        languagesMap.put("hr","Croatian");
        languagesMap.put("ht","Haitian Creole");
        languagesMap.put("hu","Hungarian");
        languagesMap.put("id","Indonesian");
        languagesMap.put("is","Icelandic");
        languagesMap.put("it","Italian");
        languagesMap.put("ja","Japanese");
        languagesMap.put("ko","Korean");
        languagesMap.put("lt","Lithuanian");
        languagesMap.put("lv","Latvian");
        languagesMap.put("mg","Malagasy");
        languagesMap.put("ms","Malay");
        languagesMap.put("mt","Maltese");
        languagesMap.put("mww","Hmong Daw");
        languagesMap.put("nb","Norwegian");
        languagesMap.put("nl","Dutch");
        languagesMap.put("otq","Querétaro Otomi");
        languagesMap.put("pl","Polish");
        languagesMap.put("pt","Portuguese");
        languagesMap.put("ro","Romanian");
        languagesMap.put("ru","Russian");
        languagesMap.put("sk","Slovak");
        languagesMap.put("sl","Slovenian");
        languagesMap.put("sm","Samoan");
        languagesMap.put("sr-Cyrl","Serbian (Cyrillic)");
        languagesMap.put("sr-Latn","Serbian (Latin)");
        languagesMap.put("sv","Swedish");
        languagesMap.put("sw","Kiswahili");
        languagesMap.put("ta","Tamil");
        languagesMap.put("th","Thai");
        languagesMap.put("tlh","Klingon");
        languagesMap.put("to","Tongan");
        languagesMap.put("tr","Turkish");
        languagesMap.put("ty","Tahitian");
        languagesMap.put("uk","Ukrainian");
        languagesMap.put("ur","Urdu");
        languagesMap.put("vi","Vietnamese");
        languagesMap.put("yua","Yucatec Maya");
        languagesMap.put("yue","Cantonese (Traditional)");
        languagesMap.put("zh-Hans","Chinese Simplified");
        languagesMap.put("zh-Hant","Chinese Traditional");
    }

    public Map<String, String> getMap() {
        return languagesMap;
    }

    public String get(String key) {
        return languagesMap.get(key);
    }

    public static Languages getInstace() {
        if(instace == null) {
            instace = new Languages();
        }
        return instace;
    }
}
