package com.dantehemerson;

public class Translation {
    private String detectedLanguage;
    private String translatedText;

    public Translation(String detectedLanguage, String translationText) {
        this.detectedLanguage = detectedLanguage;
        this.translatedText = translationText;
    }

    public String getDetectedLanguage() {
        return detectedLanguage;
    }

    public void setDetectedLanguage(String detectedLanguage) {
        this.detectedLanguage = detectedLanguage;
    }

    public boolean hasDetectedLanguage() {
        return detectedLanguage != null;
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(String translationText) {
        this.translatedText = translationText;
    }
}
