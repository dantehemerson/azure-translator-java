package com.dantehemerson;

import com.google.gson.Gson;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.*;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Azure Translator");
        frame.setContentPane(new App().getMainPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(800, 600));

        frame.pack();
        frame.setVisible(true);
    }
}