package com.dantehemerson;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.util.Map;
import java.util.TreeMap;

public class App {
    private JPanel mainPanel;
    private JTextArea textFieldToTranslate;
    private JTextArea textFieldTranslated;
    private JComboBox cbTranslateFrom;
    private JComboBox cbTranslateTo;
    private JButton translateButton;
    private JLabel info;
    private Languages languages;

    private String response;
    private String text;
    private String keyTo;
    private String keyFrom;
    private String from;
    private String to;
    private Translation translation;

    public App() {
        languages = Languages.getInstace();

        paddingFields();
        addLanguages();
        translateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                translate();
            }
        });
    }

    private void translate() {
        text = textFieldToTranslate.getText();
        text = text.trim(); // Quita los espacios en los extremos de la cadena
        // From and to languages
        Languages languages = Languages.getInstace();
        if(!text.isEmpty()) {
            keyFrom = ((ComboItem)cbTranslateFrom.getSelectedItem()).getKey();
            keyTo = ((ComboItem)cbTranslateTo.getSelectedItem()).getKey();
            if(keyTo != "detect") {
                from = languages.get(keyFrom);
            }
            to = languages.get(keyTo);

            try {
                response = Util.Translate(text, keyFrom, keyTo);
                translation = Util.getTranslation(response);

                if (translation.hasDetectedLanguage()) {
                    from = languages.get(translation.getDetectedLanguage());
                }

                info.setText("Traducido de " + from + " a " + to);

                /** Texto traducido en UI **/
                textFieldTranslated.setText(translation.getTranslatedText());
            } catch(Exception e) {
                System.out.println(e);
            }
        }
    }
    private void addLanguages() {
        Map<String, String> langsSorted = new TreeMap<>(languages.getMap()); // Sort by key

        cbTranslateFrom.addItem(new ComboItem("detect", "DETECTAR LENGUAJE" ));

        for(Map.Entry<String, String> entry : langsSorted.entrySet()) {
            cbTranslateFrom.addItem(new ComboItem(entry.getKey(), entry.getValue()));
            cbTranslateTo.addItem(new ComboItem(entry.getKey(), entry.getValue()));
        }
    }

    /**
     * Agrega padding en los textFields
     */
    private void paddingFields() {
        textFieldToTranslate.setBorder(BorderFactory.createCompoundBorder(
                textFieldToTranslate.getBorder(),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        textFieldTranslated.setBorder(BorderFactory.createCompoundBorder(
                textFieldTranslated.getBorder(),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
